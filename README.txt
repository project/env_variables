Environment variables is a simple module it will display all the environment variables available in the server.

Usage :

After install this module, we can see the menu 'Env List' in the top admin menu. By clicking this menu we can see the list of available environment variables.
By default, administrator role can access this url. If we want to access this url to different role, go to permission page and find the word 'View Environment Variables' to set the permission for different role.